FROM openjdk:8-jre
EXPOSE 8080
ADD ./target/example-1.1.jar /example.jar
ENTRYPOINT ["java", "-jar", "example.jar"]