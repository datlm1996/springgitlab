package com.datlm.domain;

import com.datlm.constant.UserType;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "user")
@Data
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "password")
    private String password;
    @Column(name = "usertype")
    @Enumerated(value = EnumType.STRING)
    private UserType userType;
}
