package com.datlm.repo;

import com.datlm.constant.UserType;
import com.datlm.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {
    @Query(value = "SELECT u FROM User AS u WHERE u.userType = :userType ORDER BY u.id DESC ")
    Optional<List<User>> findByUserType(@Param("userType") UserType userTp);
}
