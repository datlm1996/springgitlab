package com.datlm.controller;

import com.datlm.constant.UserType;
import com.datlm.domain.User;
import com.datlm.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Controller
public class HomeController {
    @Autowired
    private UserRepo userRepo;

    @PostMapping(value = "login")
    public ResponseEntity<String> test(@RequestBody User user) {
        userRepo.save(user);
        return ResponseEntity.ok("Start thanh cong");
    }

    @GetMapping(value = "get")
    public ResponseEntity<List<User>> get() {
        Optional<List<User>> userOp = userRepo.findByUserType(UserType.ADMIN);
        if (userOp.isPresent())
            return ResponseEntity.ok(userOp.get());
        return ResponseEntity.noContent().build();
    }
}
